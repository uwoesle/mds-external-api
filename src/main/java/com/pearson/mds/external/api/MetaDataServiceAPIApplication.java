package com.pearson.mds.external.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetaDataServiceAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetaDataServiceAPIApplication.class, args);
	}

}