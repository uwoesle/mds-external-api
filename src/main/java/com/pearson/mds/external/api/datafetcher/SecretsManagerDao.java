package com.pearson.mds.external.api.datafetcher;

import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClient;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SecretsManagerDao {
	private ObjectMapper mapper = new ObjectMapper();
	AWSSecretsManager secretsManager = AWSSecretsManagerClient.builder().withRegion(Regions.US_EAST_1).build();

	@SuppressWarnings("unchecked")
	public Map<String, String> readSecret(String secretId) throws Exception {

		GetSecretValueResult secretValueResult = secretsManager
				.getSecretValue(new GetSecretValueRequest().withSecretId(secretId));
		String keyValueString = secretValueResult.getSecretString();
		Map<String, String> secretsMap = mapper.readValue(keyValueString, Map.class);

		return secretsMap;
	}
}
