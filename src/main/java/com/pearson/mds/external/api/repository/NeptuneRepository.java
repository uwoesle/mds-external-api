package com.pearson.mds.external.api.repository;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.neptune.auth.NeptuneApacheHttpSigV4Signer;
import com.amazonaws.neptune.auth.NeptuneSigV4Signer;
import com.amazonaws.neptune.auth.NeptuneSigV4SignerException;

public class NeptuneRepository extends SPARQLRepository {

	private com.amazonaws.auth.AWSCredentialsProvider awsCredentialsProvider = new DefaultAWSCredentialsProviderChain();

	public NeptuneRepository(String endpointUrl) {
		super(endpointUrl);
		this.regionName = "us-east-1";
		this.endpointUrl = endpointUrl;
		this.authenticationEnabled = true;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Set up a NeptuneSparqlRepository with V4 signing enabled.
	 *
	 * @param awsCredentialsProvider the credentials provider used for
	 *                               authentication
	 * @param endpointUrl            the prefix of the Neptune endpoint (without
	 *                               "/sparql" suffix)
	 * @param regionName             name of the region in which Neptune is running
	 *
	 * @throws NeptuneSigV4SignerException in case something goes wrong with signer
	 *                                     initialization
	 */
	public NeptuneRepository(final String endpointUrl, final String regionName) throws NeptuneSigV4SignerException {

		super(getSparqlEndpoint(endpointUrl));

		this.authenticationEnabled = true;
		this.endpointUrl = endpointUrl;

		this.regionName = regionName;

		initAuthenticatingHttpClient();

	}

	/**
	 * URL of the Neptune endpoint (*without* the trailing "/sparql" servlet).
	 */
	private final String endpointUrl;

	/**
	 * The name of the region in which Neptune is running.
	 */
	private final String regionName;

	/**
	 * Whether or not authentication is enabled.
	 */
	private final boolean authenticationEnabled;

	/**
	 * The signature V4 signer used to sign the request.
	 */
	private NeptuneSigV4Signer<HttpUriRequest> v4Signer;

	/**
	 * Append the "/sparql" servlet to the endpoint URL. This is fixed, by
	 * convention in Neptune.
	 *
	 * @param endpointUrl generic endpoint/server URL
	 * @return the SPARQL endpoint URL for the given server
	 */
	private static String getSparqlEndpoint(final String endpointUrl) {
		return endpointUrl + "/sparql";
	}

	/**
	 * Wrap the HTTP client to do Signature V4 signing using Apache HTTP's
	 * interceptor mechanism.
	 *
	 * @throws NeptuneSigV4SignerException in case something goes wrong with signer
	 *                                     initialization
	 */
	protected void initAuthenticatingHttpClient() throws NeptuneSigV4SignerException {

		if (!authenticationEnabled) {
			return; // auth not initialized, no signing performed
		}

		// init an V4 signer for Apache HTTP requests
		v4Signer = new NeptuneApacheHttpSigV4Signer(regionName, awsCredentialsProvider);

		/*
		 * Set an interceptor that signs the request before sending it to the server =>
		 * note that we add our interceptor last to make sure we operate on the final
		 * version of the request as generated by the interceptor chain
		 */
		final HttpClient v4SigningClient = HttpClientBuilder.create().addInterceptorLast(new HttpRequestInterceptor() {

			@Override
			public void process(final HttpRequest req, final HttpContext ctx) throws HttpException, IOException {

				if (req instanceof HttpUriRequest) {

					final HttpUriRequest httpUriReq = (HttpUriRequest) req;
					try {
						v4Signer.signRequest(httpUriReq);
					} catch (NeptuneSigV4SignerException e) {
						throw new HttpException("Problem signing the request: ", e);
					}

				} else {

					throw new HttpException("Not an HttpUriRequest"); // this should never happen

				}
			}

		}).build();

		setHttpClient(v4SigningClient);

	}

}
