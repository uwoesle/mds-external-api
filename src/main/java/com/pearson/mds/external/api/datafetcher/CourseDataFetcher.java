package com.pearson.mds.external.api.datafetcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pearson.mds.external.api.model.Course;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class CourseDataFetcher implements DataFetcher<Course> {

	@Autowired
	public CourseDataFetcher() {

	}

	@Override
	public Course get(DataFetchingEnvironment dataFetchingEnvironment) {

		// query Neptune with SPARQL
		return null;
	}
	
	
}
