package com.pearson.mds.external.api.model;

import java.util.List;

public interface Content {

	public String getVersion();

	public void setVersion(String version);

	public String getId();

	public void setId(String id);

	public String getName();

	public void setName(String name);

	public List<Content> getContents();

	public void setContents(List<Content> contents);

}
