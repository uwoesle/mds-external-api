package com.pearson.mds.external.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.pearson.mds.external.api.datafetcher.CourseDataFetcher;

import graphql.GraphQL;
import graphql.schema.idl.RuntimeWiring;

@Service
public class CourseService {
	private static Logger logger = LoggerFactory.getLogger(CourseService.class);

	private CourseDataFetcher courseDataFetcher;

	@Value("classpath:mdsApi.graphql")
	Resource resource;

	private GraphQL graphQL;

	@Autowired
	public CourseService(CourseDataFetcher bookDataFetcher) {

		logger.info("Constructing a CourseService");
		
		this.courseDataFetcher = bookDataFetcher;
		buildRuntimeWiring();
	}

	private RuntimeWiring buildRuntimeWiring() {

		return RuntimeWiring.newRuntimeWiring().type("Query", typeWiring -> typeWiring
				.dataFetcher("course", courseDataFetcher)).build();
	}

	public GraphQL getGraphQL() {
		return graphQL;
	}
}
